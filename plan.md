# Intermediate Python Course Content

### Part 1: General / Modules / EDA

- working with IPython & Jupyter
- functions
  - `*args` & `**kwargs`
  - `return` & `yield`
- class definitions
  - private attributes/methods/variables
  - `self`
  - inheritance
- testing & error handling
  - `try`/`except`
  - `assert`
- save & load your own modules
  - `__init.py__`
  - `if __name__ == '__main__:'`
- useful modules
  - `collections`
  - `pandas`
  - `matplotlib`
  - Biopython
  - `numpy`
  - `scipy`
  - `networkx`
  - `subprocess`
  - `requests`
  - `argparse`
- managing environments & different Python versions

### Part 2: Domain-Specific Examples

#### Confirmed
- image analysis/processing with `skimage`

#### Ideas
- move Biopython section to here?
- machine learning with `sklearn`
  - applied to metagenomic data? (Georg?)
- Modeling and simulation
  - ODE/PDE (`scipy.integrate.odeint`)
  - Or perhaps GGH with `CompuCell3D` (could be done by Julio Belmonte [Leptin/Nédélec])
- Graph-based analysis & visualization
  - Analyze some biological data (could be done by Matt Rogon)
  - `networkx` could be brought into play here
- Statistics (the non-machine-learning type)
  - Hypothesis testing (`scipy.stats`)
  - Model estimation (`statsmodels`)
  - Bayesian inference (`pyMC`)
  - Good data visualization (`matplotlib`, `seaborn`)
