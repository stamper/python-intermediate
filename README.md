# Intermediate Python Programming

__In Development__

Material for an intermediate-level Python course. The first instance of the
course will be run internally, on 19&20 September 2017, at EMBL Heidelberg. 
Course instructors will include:

- Adrien Leger (Enright Group, EMBL-EBI / Marcia Group, EMBL Grenoble)
- Marco Galardini (Beltrao Group, EMBL-EBI)
- Toby Hodges (Zeller Team, EMBL Heidelberg)

Sections of the course material will be based on [lessons originally taught by
Adrien and Marco as part of the EBI pre-doc training course in summer 2016](http://www.ebi.ac.uk/%7Emarco/2016_python_course/). 
The course is being organised as part of the Bio-IT Project. 